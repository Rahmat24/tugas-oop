<?php
    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');
    
    $animal = new Animal("Shaun");
        echo "Nama =". $animal->name ."<br>" ;
        echo "Legs =". $animal->legs ."<br>" ;
        echo "Cold Blooded =". $animal->cold_blooded ."<br>". "<br>";
    
    $frog = new Frog("Budu");
        echo "Nama =". $frog->name ."<br>" ;
        echo "Legs =". $frog->legs ."<br>" ;
        echo "Cold Blooded =". $frog->cold_blooded ."<br>";
        echo "Jump =". $frog->Jump(). "<br>" . "<br>" ;

    $ape = new Ape("Kera");
        echo "Nama =". $ape->name ."<br>" ;
        echo "Legs =". $ape->legs ."<br>" ;
        echo "Cold Blooded =". $ape->cold_blooded ."<br>";
        echo "Yell =".$ape->Yell(). "<br>" . "<br>" ;

?>